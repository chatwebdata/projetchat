
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
DROP DATABASE IF EXISTS `chatweb` ;

-- -----------------------------------------------------
CREATE DATABASE IF NOT EXISTS `chatweb` DEFAULT CHARACTER SET utf8mb4 ;
USE `chatweb` ;

-- -----------------------------------------------------
-- Table `chatweb`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatweb`.`users` ;

CREATE TABLE IF NOT EXISTS `chatweb`.`users` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `pseudo` VARCHAR(45) NOT NULL COMMENT 'not null precise doit obligatoirement avoir un reseignement et unique pour être sûr que l\'on ne rentrera pas deux fois la même\',',
  `user_pwd` VARCHAR(45) NOT NULL COMMENT 'chiffrer les mpd ?',
  `email` VARCHAR(255) NOT NULL,
  `user_status` ENUM('0', '1') NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  UNIQUE INDEX `pseudo_UNIQUE` (`pseudo` ASC))
ENGINE = InnoDB;
INSERT INTO users (pseudo, user_pwd, email) VALUES ('admin', 'admin', 'admin@admin.fr');

-- -----------------------------------------------------
-- Table `chatweb`.`messages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatweb`.`messages` ;

CREATE TABLE IF NOT EXISTS `chatweb`.`messages` (
  `msg_id` INT NOT NULL AUTO_INCREMENT,
  `msg_body` VARCHAR(255) NOT NULL COMMENT 'olds messages of body\\\\ntext ou varchar ?',
  `msg_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'olds messages of create',
  `user_id` INT NOT NULL,
  PRIMARY KEY (`msg_id`, `user_id`),
  INDEX `fk_users_idx` (`user_id` ASC),
  CONSTRAINT `fk_messages_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `chatweb`.`users` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';