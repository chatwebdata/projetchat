"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");
const mysql = require('mysql');

const port = 3000;
var app = express();

var conn = mysql.createConnection({
    database: 'chatweb',
    host: "localhost",
    user: "steph",
    password: "steph"
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

conn.connect(function (err) {
    if (err) throw err;
    console.log("MySQL connected");
});

//Ajoute un nouveau message à la table messages, associé à l'id de l'utilisateur qui a rédigé le message
app.post("/sendMsg", (req, res) => {
    var selUsers = "SELECT user_id FROM users WHERE pseudo = ?";
    conn.query(selUsers, [req.body.msgNew.id], (err, rows, fields) => {
        if (err) { console.error(err); }
        let id = rows[0].user_id;
        var insMsg = "INSERT INTO messages (msg_body, user_id) VALUES ('" + req.body.msgNew.message + "','" + id + "');";
        conn.query(insMsg, (err, result) => {
            if (err) throw err;
            res.send();
        })
    })
});

//Vérifie si un utilisateur existe déjà (son pseudo ou son mail) dans la table users et l'ajoute s'il n'y apparaît pas
app.post("/addUser", (req, res) => {
    console.log(req.body);
    var selUsers = "SELECT COUNT(*) AS nb FROM users WHERE pseudo = ? OR email = ?";
    conn.query(selUsers, [req.body.id, req.body.mail], (err, rows, fields) => {
        if (err) { console.error(err); }
        if (rows[0].nb === 0) {
            var insertUser = "INSERT INTO users (pseudo, user_pwd, email, user_status) VALUES ('" + req.body.id + "','" + req.body.password + "','" + req.body.mail + "', '0');";
            conn.query(insertUser, function (err, result) {
                if (err) throw err;
                res.send();
            })
        } else {
            res.send("error");
        }
    });
    
});

//Vérifie si le pseudo et le mot de passe de l'utilisateur correspondent lors de la connexion
app.post("/connection", (req, res) => {
    var selUsers = "SELECT COUNT(*) AS nb FROM users WHERE pseudo = ? AND user_pwd = ?";
    conn.query(selUsers, [req.body.user.id, req.body.user.password], (err, rows, fields) => {
        if (err) { console.error(err); }
        if (rows[0].nb === 0) {
            res.send("error");
        } else {
            res.send();
        }
    })
});

//Modifie le user_status de la personne qui vient de se connecter au chat dans la table users
app.put("/online", (req, res) => {
    var selUsers = "UPDATE users SET user_status = '1' WHERE pseudo = ?";
    conn.query(selUsers, [req.body.user.id], (err, rows, fields) => {
        if (err) { console.error(err); }
        var selUsers = "SELECT pseudo FROM users WHERE user_status = '1'";
        conn.query(selUsers, (err, rows, fields) => {
            if (err) { console.error(err); }
            res.send(rows);
        })
    })
});

//Met à jour le user_status lorsqu'une personne se déconnecte
app.put("/offline", (req, res) => {
    var updUsers = "UPDATE users SET user_status = '0' WHERE pseudo = ?";
    conn.query(updUsers, [req.body.offlineUser], (err, rows, fields) => {
        if (err) { console.error(err); }
        res.send();
    })
});

//Renvoie les 10 derniers messages de la table messages
app.get("/messages", (req, res) => {
    var selMsg = "SELECT u.pseudo, m.msg_body, m.msg_date, m.msg_id FROM messages m JOIN users u ON m.user_id=u.user_id ORDER BY m.msg_date DESC LIMIT 10";
    conn.query(selMsg, (err, rows, fields) => {
        if (err) { console.error(err); }
        res.send(rows);
    })
});

//Renvoie un tableau contenant les pseudos des utilisateurs connectés
app.get("/onlineUsers", (req, res) => {
    var selUsers = "SELECT pseudo FROM users WHERE user_status = '1'";
    conn.query(selUsers, (err, rows, fields) => {
        if (err) { console.error(err); }
        res.send(rows);
    })
});

//Renvoie toutes les informations contenues dans la table users
app.get("/users", (req, res) => {
    var selUsers = "SELECT * FROM users ORDER BY pseudo";
    conn.query(selUsers, (err, rows, fields) => {
        if (err) { console.error(err); }
        res.send(rows);
    })
});

//Modifie la table users avec les informations que l'administrateur a édité dans le panneau d'administration
app.put("/user/:realId", (req, res) => {
    var updUser = "UPDATE users SET pseudo = ?, email = ?, user_pwd = ? WHERE user_id = ?";
    conn.query(updUser, [req.body.pseudo, req.body.mail, req.body.password, req.params.realId], (err, rows, fields) => {
        if (err) {
            console.error(err);
            res.send("error");
        } else {
            var selUsers = "SELECT * FROM users ORDER BY pseudo";
            conn.query(selUsers, [req.params.realId], (err, rows, fields) => {
                if (err) { console.error(err); }
                res.send(rows);
            })
        }
    })
});

//Supprime un utilisateur et ses messages
app.delete("/user/:id", (req, res) => {
    req.params.id;
    var delUser = "DELETE FROM users WHERE user_id = " + req.params.id + ";";
    conn.query(delUser, (err, rows, fields) => {
        if (err) { console.error(err); }
        res.send();
    })
});

app.listen(port);