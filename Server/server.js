"use strict";

const axios = require("axios");
const express = require("express");
const bodyParser = require("body-parser");

const apiUrl = "http://localhost:3000";
const port = 8080;
var app = express();

app.use(express.static("css"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view-engine', 'ejs');

//Affiche la page index sur /
app.get('/', (req, res) => {
    res.render("index.ejs", {message: ""});
})

//Affiche la page de formulaire sur /form
app.get("/form", (req, res) => {
    res.render("form.ejs", { message: ""});
});

//Affiche la page de tchat en injectant à la page le pseudo de l'utilisateur, le tableau d'utilisateur en ligne et le tableau des 10 derniers messages
app.get("/online/:id", (req, res) => {
    const onlineUser = req.params.id;
    axios.get(apiUrl + "/messages")
        .then (response => {
            const tabMsg = response.data;
            axios.get(apiUrl + "/onlineUsers")
                .then (resp => {
                    const tabOnline = resp.data;
                    res.render("chat.ejs", { onlineUser, tabOnline, tabMsg });
                })
                .catch (function (error) {
                    console.log(error);
                })
        })
})

//Affiche la page d'administration avec les donnéees de tous les utilisateurs
app.post("/admin", (req,res) => {
    axios.get(apiUrl + "/users")
        .then (response => {
            const tabUsers = response.data;
            res.render("admin.ejs", { tabUsers });
        })
})

//Ajoute un nouvel utilisateur à la base de données users lors d'une validation du formulaire d'inscription (avec vérification préalable de l'existence dans la base de données du pseudo et/ou mail)
app.post("/addUser", (req, res) => {
    const user = req.body;
    axios.post(apiUrl + "/addUser", user)
        .then (response => {
            const msg = response.data;
            if (msg === "error") {
                res.render("form.ejs", { message: user.id + " : pseudonyme ou e-mail déjà utilisé."})
            } else {
                //TODO : comparer les deux pass
                res.render("index.ejs", { message: "Inscription validée" });
            }
        })
});

//Récupère le message posté et affiche la page de chat actualisée
app.post("/sendMsg", (req, res) => {
    const msgNew = req.body;
    const onlineUser = msgNew.id;
    axios.post(apiUrl + "/sendMsg", {msgNew})
        .then (respons => {
            res.redirect("/online/" + onlineUser);
        })
});

//Lorsque l'utilisateur se connecte au chat, vérifie le mot de passe et affiche soit un message d'erreur soit la page du chat 
app.post("/online", (req, res) => {
    const user = req.body;
    const onlineUser = req.body.id;
    axios.post(apiUrl + "/connection", { user })
        .then (resp => {
            if (resp.data === "error") {
                let message = "Le pseudo et/ou le mot de passe est erroné.";
                res.render("index.ejs", { message });
            } else {
                axios.get(apiUrl + "/messages")
                    .then (respons => {
                        const tabMsg = respons.data;
                        console.log = tabMsg;
                        axios.put(apiUrl + "/online", { user })
                            .then(response => {
                                const tabOnline = response.data;
                                res.render("chat.ejs", { onlineUser, tabOnline, tabMsg });
                            })
                    })
            }
        })
});

//Envoie à l'API le pseudo de la personne qui se déconnecte et affiche la page d'accueil
app.get("/offline/:id", (req, res) => {
    const offlineUser = req.params.id;
    axios.put(apiUrl + "/offline/", {offlineUser});
    res.render("index.ejs", { message: "" });
});

//Récupère les informations contenues dans le tableau modifié par l'administrateur et les envoie à l'API pour enregistrer les modifications, puis affiche la page d'administration actualisée
app.post("/user/:id", (req, res) => {
    const user = req.params.id;
    const dataForm = req.body;
    axios.put(apiUrl + "/user/" + user, dataForm)
        .then(response => {
            const tabUsers = response.data;
            res.render("admin.ejs", { tabUsers });
        });
});

//Récupère le pseudo de l'utilisateur à supprimer (mise à jour de la table users) et affiche la page d'administration avec le nouveau tableau d'utilisateurs
app.get("/user/:id", (req, res) => {
    const deletedName = req.params.id;
    axios.delete(apiUrl + "/user/" + deletedName)
        .then (response => {
            axios.get(apiUrl + "/users")
                .then (resp => {
                    const tabUsers = resp.data;
                    res.render("admin.ejs", { tabUsers });
                })
        })
});

app.listen(port);